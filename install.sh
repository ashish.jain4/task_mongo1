os=$(grep  ^NAME /etc/*release)

if [[ $os = '/etc/os-release:NAME="Amazon Linux"' ]] ;then

        if [ -f /etc/yum.repos.d/mongodb-org-5.0.repo ] ;then
                sudo yum install -y mongodb
                echo "Mongo Db has been installed"         

        else
                sudo yum -y update
                touch /etc/yum.repos.d/mongodb-org-5.0.repo
                echo '[mongodb-org-5.0]' >> /etc/yum.repos.d/mongodb-org-5.0.repo
                echo 'name=MongoDB Repository' >> /etc/yum.repos.d/mongodb-org-5.0.repo
                echo "baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/5.0/x86_64/" >> /etc/yum.repos.d/mongodb-org-5.0.repo
                echo 'gpgcheck=1' >> /etc/yum.repos.d/mongodb-org-5.0.repo
                echo 'enabled=1' >> /etc/yum.repos.d/mongodb-org-5.0.repo
                echo 'gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc'  >> /etc/yum.repos.d/mongodb-org-5.0.repo              
                sudo yum install -y mongodb-org
                echo "Mongo Db has been installed"
        fi

elif [[ $os = '/etc/os-release:NAME="Ubuntu"' ]] ;then
        sudo apt update
        sudo apt -y upgrade
        sudo apt install mongodb -y
fi
